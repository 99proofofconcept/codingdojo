package ObserverPattern;

public class AirportScreen implements Screen {

    private final FlightNotification flightNotification;
    private final String screenName;
    private String flightInformation;
    
    public AirportScreen(FlightNotification flightNotification, String screenName)
    {
        this.flightNotification = flightNotification;
        this.screenName = screenName;
        flightNotification.add(this);
    }

    private void printInformation()
    {
        System.out.println(flightInformation);
    }

    private void updateInformation()
    {
        flightInformation = screenName + " ::: " + flightNotification.getDescription() + " -- "  + flightNotification.getArrival();
    }

    @Override
    public void update()
    {
        updateInformation();
        printInformation();
    }

    public void removeFlight()
    {
        flightNotification.remove(this);
    }
}
